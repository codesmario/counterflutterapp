import 'package:flutter/material.dart';

class CounterScreen extends StatefulWidget {
  const CounterScreen({super.key});

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  int counter = 10;
  @override
  Widget build(BuildContext context) {
    const fontSize30 = TextStyle(fontSize: 30, color: Colors.black26);
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      appBar: AppBar(
        backgroundColor: Colors.green.shade400,
        title: const Text('CounterScreen'),
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            const Text('Hello there', style: fontSize30),
                  Text( '$counter', style: fontSize30),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FloatingActionButton(
            child: const Icon( Icons.exposure_plus_1_outlined ),
            onPressed: () { 
                counter++;
              setState(() {});
            },
          ),
          FloatingActionButton(
            child: const Icon( Icons.exposure_zero_outlined ),
            onPressed: () {
                counter = 0;
              setState(() {});
            },
          ),
          FloatingActionButton(
            child: const Icon( Icons.exposure_minus_1_outlined ),
            onPressed: () { 
                counter--;
              setState(() {});
            },
          ),
        ],
      ),
      
    );
  }
}